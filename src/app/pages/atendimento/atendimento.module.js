/**
 * @author Rodolfo Bernardo
 * created on 11.09.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.atendimento', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('atendimento', {
          url: '/atendimento',
          templateUrl: 'app/pages/atendimento/atendimento.html',
          controller: 'AtendimentoPageCtrl',
          title: 'Atendimento',
          sidebarMeta: {
            icon: 'ion-android-call',
            order: 200,
          },
        });
  }

})();
