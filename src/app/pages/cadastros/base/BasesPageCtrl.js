/**
 * @author Rodolfo Bernardo
 * created on 18.08.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.cadastros')
      .controller('BasesPageCtrl', BasesPageCtrl);
 BasesPageCtrl.$inject = ['$scope', '$filter', 'editableOptions', 'editableThemes', '$uibModal','BasesPageService'];
  /** @ngInject */
  function BasesPageCtrl($scope, $filter, editableOptions, editableThemes, $uibModal, basesPageService) {
    console.debug('BasesPageCtrl Inicio');

    $scope.open = function (page, size) {
      $uibModal.open({
        animation: true,
        templateUrl: page,
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });
    };

    $scope.smartTableData = [];
    $scope.smartTablePageSize = 5;

    basesPageService.getAll().success(function (data) {
      console.log(data);
      $scope.smartTableData.push(data);
      $scope.editableTableData = $scope.smartTableData.slice(0, 36);
      $rootScope.$apply();
    }).error(function (error) {console.error(error);});

    $scope.showGroup = function(user) {
      if(user.group && $scope.groups.length) {
        var selected = $filter('filter')($scope.groups, {id: user.group});
        return selected.length ? selected[0].text : 'Not set';
      } else return 'Not set'
    };

    $scope.showStatus = function(user) {
      var selected = [];
      if(user.status) {
        selected = $filter('filter')($scope.statuses, {value: user.status});
      }
      return selected.length ? selected[0].text : 'Not set';
    };
  }

})();
