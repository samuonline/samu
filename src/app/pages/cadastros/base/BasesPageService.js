/**
 * @author Rodolfo Bernardo
 * created on 18.08.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.cadastros')
      .service('BasesPageService', BasesPageService);

 
  /** @ngInject */
  function BasesPageService($http, $q, config) {
    console.debug('BasesPageService Inicio');

     var self = this;
        this.getById = GetById;
        this.getAll = GetAll;


        //////////////////////(URL´s)/////////////////////////////////
        var API = config.baseUrl + '/Base_Samu/';

         //START GETALL
        function GetAll() {
            var config = {
                method: 'GET',
                url: API,
                cache: false
            };
            return $http(config);
        };//END GETALL

        function GetById(id){
            var config = {
                method: 'GET',
                url: API + id,
                cache: false
            };
            return $http(config);
        };
  }

})();
