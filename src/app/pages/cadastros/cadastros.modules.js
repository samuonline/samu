/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.cadastros', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('cadastros', {
          url: '/cadastros',
          templateUrl: 'app/pages/cadastros/cadastros.html',
          abstract: true,
          title: 'Cadastros',
          sidebarMeta: {
            icon: 'ion-compose',
            order: 100,
          },
        })
        .state('cadastros.usuarios', {
          url: '/usuarios',
          templateUrl: 'app/pages/cadastros/usuarios/usuario.html',
          controller: 'UsuariosPageCtrl',
          title: 'Usuários',
          sidebarMeta: {
            order: 0,
          },
        })
        .state('cadastros.bases', {
          url: '/bases',
          templateUrl: 'app/pages/cadastros/base/base.html',
          controller: 'BasesPageCtrl',
          title: 'Bases',
          sidebarMeta: {
            order: 100,
          },
        })
        .state('cadastros.viaturas', {
          url: '/viaturas',
          templateUrl: 'app/pages/cadastros/viaturas/viatura.html',
          controller: 'ViaturasPageCtrl',
          title: 'Viaturas',
          sidebarMeta: {
            order: 200,
          },
        })
  }

})();