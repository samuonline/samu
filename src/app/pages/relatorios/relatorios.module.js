/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.relatorios', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('relatorios', {
          url: '/relatorios',
          template : '<ui-view></ui-view>',
          abstract: true,
          controller: 'RelatoriosPageCtrl',
          title: 'Relatórios',
          sidebarMeta: {
            icon: 'ion-ios-printer-outline',
            order: 300,
          },
        }).state('relatorios.atendimentos', {
          url: '/ratendimentos',
          templateUrl: 'app/pages/relatorios/basic/tables.html',
          title: 'Atendimentos',
          sidebarMeta: {
            order: 0,
          },
        });
    $urlRouterProvider.when('/relatorios','/relatorios/basic');
  }

})();
