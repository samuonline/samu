USE [master]
GO
/****** Object:  Database [Samu_Online]    Script Date: 29/09/2016 20:44:58 ******/
CREATE DATABASE [Samu_Online]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Samu_Online', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Samu_Online.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Samu_Online_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Samu_Online_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Samu_Online] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Samu_Online].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Samu_Online] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Samu_Online] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Samu_Online] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Samu_Online] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Samu_Online] SET ARITHABORT OFF 
GO
ALTER DATABASE [Samu_Online] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Samu_Online] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Samu_Online] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Samu_Online] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Samu_Online] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Samu_Online] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Samu_Online] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Samu_Online] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Samu_Online] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Samu_Online] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Samu_Online] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Samu_Online] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Samu_Online] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Samu_Online] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Samu_Online] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Samu_Online] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Samu_Online] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Samu_Online] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Samu_Online] SET  MULTI_USER 
GO
ALTER DATABASE [Samu_Online] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Samu_Online] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Samu_Online] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Samu_Online] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Samu_Online] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Samu_Online]
GO
/****** Object:  User [Samu]    Script Date: 29/09/2016 20:45:01 ******/
CREATE USER [Samu] FOR LOGIN [Samu] WITH DEFAULT_SCHEMA=[db_accessadmin]
GO
ALTER ROLE [db_owner] ADD MEMBER [Samu]
GO
/****** Object:  Table [dbo].[Atendente]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Atendente](
	[Usuario_Cod_Usuario] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Base_Samu]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Samu](
	[Cod_Base_Samu] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](100) NULL,
	[Cep] [varchar](8) NULL,
	[Logradouro] [varchar](45) NULL,
	[Bairro] [varchar](45) NULL,
	[Cidade] [varchar](45) NULL,
	[Estado] [varchar](2) NULL,
	[Complemento] [varchar](45) NULL,
	[Numero] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Base_Samu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [Cod_Base_Samu_UNIQUE] UNIQUE NONCLUSTERED 
(
	[Cod_Base_Samu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Chamado]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Chamado](
	[Nr_Telefone] [varchar](11) NOT NULL,
	[Hra_Inicio] [time](0) NOT NULL,
	[Hra_Fim] [time](0) NULL,
	[Medico_Usuario_Cod_Usuario] [int] NOT NULL,
	[Atendente_Usuario_Cod_Usuario] [int] NOT NULL,
	[Cod_Chamado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Chamado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Enfermeiro]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Enfermeiro](
	[Coren] [varchar](10) NOT NULL,
	[Usuario_Cod_Usuario] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Coren] ASC,
	[Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Formulario_Atendimento]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Formulario_Atendimento](
	[Cod_Formulario] [int] NOT NULL,
	[Ficha_APH] [varchar](45) NULL,
	[Preenche] [int] NOT NULL,
	[Chamado_Cod_Chamado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Formulario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lista_Negra]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lista_Negra](
	[Nr_Telefone] [varchar](11) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Nr_Telefone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Medico]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Medico](
	[CRM] [varchar](10) NOT NULL,
	[Usuario_Cod_Usuario] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Paciente]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Paciente](
	[Cod_Paciente] [int] NOT NULL,
	[Nome] [varchar](45) NULL,
	[Sexo] [char](1) NULL,
	[Idade] [int] NULL,
	[Chamado_Cod_Chamado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Paciente] ASC,
	[Chamado_Cod_Chamado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Socorre]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Socorre](
	[Medico_Usuario_Cod_Usuario] [int] NOT NULL,
	[Enfermeiro_Coren] [varchar](10) NOT NULL,
	[Enfermeiro_Usuario_Cod_Usuario] [int] NOT NULL,
	[Chamado_Cod_Chamado] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[Cod_Usuario] [int] NOT NULL,
	[Nome] [varchar](50) NULL,
	[Senha] [varchar](50) NOT NULL,
	[Cpf] [varchar](11) NOT NULL,
	[Skype] [varchar](50) NOT NULL,
	[Tipo] [int] NOT NULL,
	[Ativo] [smallint] NOT NULL,
	[Base_Samu_Cod_Base_Samu] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Viatura]    Script Date: 29/09/2016 20:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Viatura](
	[Cod_Viatura] [int] NOT NULL,
	[Placa] [varchar](7) NOT NULL,
	[Tipo] [int] NOT NULL,
	[Ano] [varchar](4) NOT NULL,
	[Skype] [varchar](45) NULL,
	[Cod_Base_Samu] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Viatura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [fk_Chamado_Atendente2_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Chamado_Atendente2_idx] ON [dbo].[Chamado]
(
	[Atendente_Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Chamado_Medico2_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Chamado_Medico2_idx] ON [dbo].[Chamado]
(
	[Medico_Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Enfermeiro_Usuario1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Enfermeiro_Usuario1_idx] ON [dbo].[Enfermeiro]
(
	[Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Formulario_Atendimento_Chamado1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Formulario_Atendimento_Chamado1_idx] ON [dbo].[Formulario_Atendimento]
(
	[Chamado_Cod_Chamado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Formulario_Atendimento_Medico1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Formulario_Atendimento_Medico1_idx] ON [dbo].[Formulario_Atendimento]
(
	[Preenche] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Medico_Usuario1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Medico_Usuario1_idx] ON [dbo].[Medico]
(
	[Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Paciente_Chamado1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Paciente_Chamado1_idx] ON [dbo].[Paciente]
(
	[Chamado_Cod_Chamado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Socorre_Chamado1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Socorre_Chamado1_idx] ON [dbo].[Socorre]
(
	[Chamado_Cod_Chamado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [fk_Socorre_Enfermeiro1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Socorre_Enfermeiro1_idx] ON [dbo].[Socorre]
(
	[Enfermeiro_Coren] ASC,
	[Enfermeiro_Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Socorre_Medico1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Socorre_Medico1_idx] ON [dbo].[Socorre]
(
	[Medico_Usuario_Cod_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_Usuario_Base_Samu1_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [fk_Usuario_Base_Samu1_idx] ON [dbo].[Usuario]
(
	[Base_Samu_Cod_Base_Samu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Cod_Base_Samu_Viatura_idx]    Script Date: 29/09/2016 20:45:02 ******/
CREATE NONCLUSTERED INDEX [Cod_Base_Samu_Viatura_idx] ON [dbo].[Viatura]
(
	[Cod_Base_Samu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Atendente]  WITH CHECK ADD  CONSTRAINT [fk_Atendente_Usuario1] FOREIGN KEY([Usuario_Cod_Usuario])
REFERENCES [dbo].[Usuario] ([Cod_Usuario])
GO
ALTER TABLE [dbo].[Atendente] CHECK CONSTRAINT [fk_Atendente_Usuario1]
GO
ALTER TABLE [dbo].[Chamado]  WITH CHECK ADD  CONSTRAINT [fk_Chamado_Atendente2] FOREIGN KEY([Atendente_Usuario_Cod_Usuario])
REFERENCES [dbo].[Atendente] ([Usuario_Cod_Usuario])
GO
ALTER TABLE [dbo].[Chamado] CHECK CONSTRAINT [fk_Chamado_Atendente2]
GO
ALTER TABLE [dbo].[Chamado]  WITH CHECK ADD  CONSTRAINT [fk_Chamado_Medico2] FOREIGN KEY([Medico_Usuario_Cod_Usuario])
REFERENCES [dbo].[Medico] ([Usuario_Cod_Usuario])
GO
ALTER TABLE [dbo].[Chamado] CHECK CONSTRAINT [fk_Chamado_Medico2]
GO
ALTER TABLE [dbo].[Enfermeiro]  WITH CHECK ADD  CONSTRAINT [fk_Enfermeiro_Usuario1] FOREIGN KEY([Usuario_Cod_Usuario])
REFERENCES [dbo].[Usuario] ([Cod_Usuario])
GO
ALTER TABLE [dbo].[Enfermeiro] CHECK CONSTRAINT [fk_Enfermeiro_Usuario1]
GO
ALTER TABLE [dbo].[Formulario_Atendimento]  WITH CHECK ADD  CONSTRAINT [fk_Formulario_Atendimento_Chamado1] FOREIGN KEY([Chamado_Cod_Chamado])
REFERENCES [dbo].[Chamado] ([Cod_Chamado])
GO
ALTER TABLE [dbo].[Formulario_Atendimento] CHECK CONSTRAINT [fk_Formulario_Atendimento_Chamado1]
GO
ALTER TABLE [dbo].[Formulario_Atendimento]  WITH CHECK ADD  CONSTRAINT [fk_Formulario_Atendimento_Medico1] FOREIGN KEY([Preenche])
REFERENCES [dbo].[Medico] ([Usuario_Cod_Usuario])
GO
ALTER TABLE [dbo].[Formulario_Atendimento] CHECK CONSTRAINT [fk_Formulario_Atendimento_Medico1]
GO
ALTER TABLE [dbo].[Medico]  WITH CHECK ADD  CONSTRAINT [fk_Medico_Usuario1] FOREIGN KEY([Usuario_Cod_Usuario])
REFERENCES [dbo].[Usuario] ([Cod_Usuario])
GO
ALTER TABLE [dbo].[Medico] CHECK CONSTRAINT [fk_Medico_Usuario1]
GO
ALTER TABLE [dbo].[Paciente]  WITH CHECK ADD  CONSTRAINT [fk_Paciente_Chamado1] FOREIGN KEY([Chamado_Cod_Chamado])
REFERENCES [dbo].[Chamado] ([Cod_Chamado])
GO
ALTER TABLE [dbo].[Paciente] CHECK CONSTRAINT [fk_Paciente_Chamado1]
GO
ALTER TABLE [dbo].[Socorre]  WITH CHECK ADD  CONSTRAINT [fk_Socorre_Chamado1] FOREIGN KEY([Chamado_Cod_Chamado])
REFERENCES [dbo].[Chamado] ([Cod_Chamado])
GO
ALTER TABLE [dbo].[Socorre] CHECK CONSTRAINT [fk_Socorre_Chamado1]
GO
ALTER TABLE [dbo].[Socorre]  WITH CHECK ADD  CONSTRAINT [fk_Socorre_Enfermeiro1] FOREIGN KEY([Enfermeiro_Coren], [Enfermeiro_Usuario_Cod_Usuario])
REFERENCES [dbo].[Enfermeiro] ([Coren], [Usuario_Cod_Usuario])
GO
ALTER TABLE [dbo].[Socorre] CHECK CONSTRAINT [fk_Socorre_Enfermeiro1]
GO
ALTER TABLE [dbo].[Socorre]  WITH CHECK ADD  CONSTRAINT [fk_Socorre_Medico1] FOREIGN KEY([Medico_Usuario_Cod_Usuario])
REFERENCES [dbo].[Medico] ([Usuario_Cod_Usuario])
GO
ALTER TABLE [dbo].[Socorre] CHECK CONSTRAINT [fk_Socorre_Medico1]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [fk_Usuario_Base_Samu1] FOREIGN KEY([Base_Samu_Cod_Base_Samu])
REFERENCES [dbo].[Base_Samu] ([Cod_Base_Samu])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [fk_Usuario_Base_Samu1]
GO
ALTER TABLE [dbo].[Viatura]  WITH CHECK ADD  CONSTRAINT [Cod_Base_Samu_Viatura] FOREIGN KEY([Cod_Base_Samu])
REFERENCES [dbo].[Base_Samu] ([Cod_Base_Samu])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Viatura] CHECK CONSTRAINT [Cod_Base_Samu_Viatura]
GO
USE [master]
GO
ALTER DATABASE [Samu_Online] SET  READ_WRITE 
GO
