﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplication.Data;
namespace Aplication.Service.Services
{
    public interface IAtendenteService
    {
        List<Atendente> GetAtendentes();
        Atendente GetAtendente(int id);
    }
}
