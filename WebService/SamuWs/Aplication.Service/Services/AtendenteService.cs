﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aplication.Data;
using System.Data;

namespace Aplication.Service.Services
{
    public class AtendenteService : IAtendenteService
    {
        private Samu_OnlineEntities db = new Samu_OnlineEntities();
        public Atendente GetAtendente(int id)
        {
            return db.Atendente.Find(id);
        }

        public List<Atendente> GetAtendentes()
        {
            return db.Atendente.ToList();
        }
    }
}