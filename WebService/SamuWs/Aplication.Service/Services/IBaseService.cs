﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aplication.Data;

namespace Aplication.Service.Services
{
    public interface IBaseService
    {
        List<Base_Samu> GetBase_Samu();
        Base_Samu GetBase_Samu(int id);
    }
}