﻿using Aplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplication.Service.Services
{
    public class BaseService : IBaseService
    {
        private Samu_OnlineEntities db = new Samu_OnlineEntities();
        public Base_Samu GetBase_Samu(int id)
        {
            return db.Base_Samu.Find(id);
        }

        public List<Base_Samu> GetBase_Samu()
        {
            return db.Base_Samu.ToList();
        }
    }
}