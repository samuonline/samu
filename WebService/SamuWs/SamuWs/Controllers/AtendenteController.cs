﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Aplication.Service.Services;
using Aplication.Data;

namespace SamuWs.Controllers
{
    public class AtendenteController : ApiController
    {
        private IAtendenteService atendenteService = new AtendenteService();

        public IEnumerable<Atendente> Get()
        {
            return atendenteService.GetAtendentes();
        }
        public IHttpActionResult Get(int id)
        {
            var atendente = atendenteService.GetAtendente(id);
            if(atendente == null)
            {
                return NotFound();
            }
            return Ok(atendente);
        }

    }
}
