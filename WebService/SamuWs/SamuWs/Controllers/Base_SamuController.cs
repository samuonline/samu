﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Aplication.Data;
using Aplication.Service.Services;
using System.Web.Http.Cors;

namespace SamuWs.Controllers
{
    public class Base_SamuController : ApiController
    {
        private IBaseService baseService = new BaseService();
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IEnumerable<Base_Samu> Get()
        {
            return baseService.GetBase_Samu();
        }
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult Get(int id)
        {
            var base_samu = baseService.GetBase_Samu(id);
            if (base_samu == null)
            {
                return NotFound();
            }
            return Ok(base_samu);
        }

    }
}
